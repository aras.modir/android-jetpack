package com.modir.androidjetpack.databinding;

public interface IMainActivity {

    void inflateQuantityDialog();

    void setQuantity(int quantity);

}
