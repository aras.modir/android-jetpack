package com.modir.androidjetpack.navigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.modir.androidjetpack.R

class NavigationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
    }
}
