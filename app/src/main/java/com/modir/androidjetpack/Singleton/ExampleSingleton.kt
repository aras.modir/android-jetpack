package com.modir.androidjetpack.Singleton

import com.modir.androidjetpack.Singleton.models.User

object ExampleSingleton {
    val singletonUser: User by lazy {
        User("test@gmail.com", "aras", "image.png")
    }
}