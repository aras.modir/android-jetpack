package com.modir.androidjetpack.Singleton.mainviewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.modir.androidjetpack.Singleton.models.User
import com.modir.androidjetpack.Singleton.repository.MainRepository

class MainViewModel : ViewModel() {

    private val _userId: MutableLiveData<String> = MutableLiveData()

    val user: LiveData<User> = Transformations
            .switchMap(_userId) {
                MainRepository.getUser(it)
            }

    fun setUserId(userId: String) {
        val update = userId
        if (_userId.value == update) {
            return
        }
        _userId.value = update
    }

    fun cancelJobs() {
        MainRepository.cancelJobs()
    }
}