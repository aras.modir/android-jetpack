package com.modir.androidjetpack.Singleton

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.modir.androidjetpack.R
import com.modir.androidjetpack.Singleton.mainviewmodel.MainViewModel

class SingletonActivity : AppCompatActivity() {

    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_singleton)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.user.observe(this, Observer {
            println("Debug: $it")
        })

        viewModel.setUserId("1")
//        println("Debug: ${ExampleSingleton.singletonUser.hashCode()}")
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.cancelJobs()
    }
}
